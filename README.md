#SCPI - Self contained plankton imager

See the new [SeeStar page](http://bitbucket.org/mbari/seestar) for info...
![SCPI prototype](https://bitbucket.org/beroe/scpi/raw/master/images/SCPI_Sys_01b.JPG "Prototype modules")
